require 'discordrb'

token = open("bot_token").each_line.take(1).last.chop 

bot = Discordrb::Bot.new token: token
bot.reaction_add do |event|
  if event.emoji.name == '📌' then
    event.message.pin
  end
end

bot.reaction_remove do |event|
  if event.emoji.name == '📌' then
    if event.message.pinned then
      event.message.unpin
    end
  end
end

bot.run
