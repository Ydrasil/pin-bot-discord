def get_token()
  puts "Enter your bot token or leave empty and fill 'token_bot' file later:"
  token = gets
  return token
end

file = "bot_token"

if !File.exists?(file) or File.empty?(file) then
  File.open(file, "w") do |f|
    token = get_token
    if !token.empty? then
      f.write(token)
    end
  end
else
      puts("'token_bot' isn't empty, if you want to change the token, remove the file and restart init_bot.rb")
end
